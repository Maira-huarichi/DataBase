create database comercial;
drop database comercial;

create table cliente(
	nombre varchar(15),
	apellido varchar(15),
	edad int
);

drop table cliente;
alter table cliente add edad int;

create table producto(
	nombre_producto varchar(15),
	precio int
);

create table ventas(	
	cantidad_productos int 
);

drop table ventas;
drop table cliente;
drop table productos;

select *
from cliente;

exec sp_rename 'cliente.apellido', 'apellidos', 'COLUMN';

insert into cliente values('maira','huarachi',28);
insert into cliente values('isabel','huarachi',25);
insert into cliente values('ronald','Pinto',25);
insert into cliente values('vanesa','delgadillo',25);
insert into cliente values('marcelo','carpio',22);
insert into cliente values('diego','guzman',28);
insert into cliente values('ruben','costas',15);

delete from cliente;

insert into producto values('mochila',45);
insert into producto values('radio',65);
insert into producto values('televisor',85);
insert into producto values('silla',45);
insert into producto values('mesa',90);

insert into ventas values(5);
insert into ventas values(6);
insert into ventas values(2);
insert into ventas values(3);
insert into ventas values(1);

select *
from ventas;

select nombre,edad
from cliente
group by edad,nombre;

select *
from cliente
where edad >= 20;

delete from cliente 
where edad >=20;